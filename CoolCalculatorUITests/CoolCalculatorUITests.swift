//
//  CoolCalculatorUITests.swift
//  CoolCalculatorUITests
//
//  Created by Admin on 01.05.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import XCTest

class CoolCalculatorUITests: XCTestCase {
    let app = XCUIApplication()

    override func setUp() {
        super.setUp()

        continueAfterFailure = false

        XCUIApplication().launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func tapButtonWithText(_ text: String)
    {
        button(text).tap()
    }

    func tapButton(_ button: XCUIElement)
    {
        button.tap()
    }

    func button(_ text: String) -> XCUIElement {
        return XCUIApplication().buttons[text]
    }


    func assertLabelTextEquals(_ text: String)
    {
        XCTAssertEqual(app.staticTexts.element(matching: .any, identifier: "resultLabel").label, text)
    }

    func clear()
    {
        button("C").tap()
    }

    func testNumbersButtons() {
        tapButtonWithText("0")
        assertLabelTextEquals("0")

        clear()

        tapButtonWithText("1")
        assertLabelTextEquals("1")

        clear()

        tapButtonWithText("2")
        assertLabelTextEquals("2")

        clear()

        tapButtonWithText("3")
        assertLabelTextEquals("3")

        clear()

        tapButtonWithText("4")
        assertLabelTextEquals("4")

        clear()

        tapButtonWithText("5")
        assertLabelTextEquals("5")

        clear()

        tapButtonWithText("6")
        assertLabelTextEquals("6")

        clear()

        tapButtonWithText("7")
        assertLabelTextEquals("7")

        clear()

        tapButtonWithText("8")
        assertLabelTextEquals("8")

        clear()

        tapButtonWithText("9")
        assertLabelTextEquals("9")
        
        clear()
    }

    func testSubraction() {
        let firstNumber = randomDouble()
        let secondNumber = randomDouble()
        inputNumber(firstNumber)
        subtract()
        inputNumber(secondNumber)
        equalsButtonClick()
        let result = String(Double(firstNumber - secondNumber))
        assertLabelTextEquals(result)
    }
    
    func testAddition()
    {
        let firstNumber = randomDouble()
        let secondNumber = randomDouble()
        inputNumber(firstNumber)
        plus()
        inputNumber(secondNumber)
        equalsButtonClick()
        let result = String(Double(firstNumber + secondNumber))
        assertLabelTextEquals(result)
    }
    
    func testDivision()
    {
        let firstNumber = randomDouble()
        let secondNumber = randomDouble()
        inputNumber(firstNumber)
        divide()
        inputNumber(secondNumber)
        equalsButtonClick()
        let result = String(Double(firstNumber / secondNumber))
        assertLabelTextEquals(result)
    }
    
    func testMultiply()
    {
        let firstNumber = randomDouble()
        let secondNumber = randomDouble()
        inputNumber(firstNumber)
        multiply()
        inputNumber(secondNumber)
        equalsButtonClick()
        let result = String(Double(firstNumber * secondNumber))
        assertLabelTextEquals(result)
    }

    func inputNumber(_ number: Double)
    {
        var character: String = ""

        let numberString = String(number)

        for i in 0..<numberString.count
        {
            let index = numberString.index(numberString.startIndex, offsetBy: i)
            character = String(numberString[index])
            
            if(character == ".")
            {
                tapButtonWithText(",")
            }
            else
            {
                tapButtonWithText(character)
            }
        }
    }

    func subtract() {
        button("-").tap()
    }
    
    func plus() {
        button("+").tap()
    }
    
    func divide() {
        button("/").tap()
    }
    
    func multiply() {
        button("x").tap()
    }
    
    func equalsButtonClick()
    {
        button("=").tap()
    }
    
    func randomDouble() -> Double {
        return round((Double(arc4random_uniform(1000)) / 10000) * 100) / 100
    }
}
