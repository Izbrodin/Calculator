import Foundation

enum Operations: CustomStringConvertible
{
    case Multiply
    case Divide
    case Plus
    case Subtract
    
    var description: String {
        switch self
        {
        case .Multiply: return "x"
        case .Divide: return "/"
        case .Plus: return "+"
        case .Subtract: return "-"
        }
    }
}
