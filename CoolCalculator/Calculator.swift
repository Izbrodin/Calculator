import Foundation

class Calculator
{
    class func multiply(_ number: Double, _ multiplier: Double) -> Double {
        return number * multiplier
    }

    class func divide(_ number: Double, _ divisor: Double) -> Double {
        return number / divisor
    }

    class func plus(_ number: Double, _ secondNumber: Double) -> Double {
        return number + secondNumber
    }

    class func subtract(_ number: Double, _ secondNumber: Double) -> Double {
        return number - secondNumber
    }

    class func calculatePercent(howMany percent: Double, from number: Double) -> Double {
        return percent / 100 * number
    }

    class func plusPercent(_ number: Double, _ percentToAdd: Double) -> Double {
        return plus(number, calculatePercent(howMany: percentToAdd, from: number))
    }

    class func subtractPercent(_ number: Double, _ percentToSubtract: Double) -> Double {
        return subtract(number, calculatePercent(howMany: percentToSubtract, from: number))
    }
    
    class func inverse(_ number: Double) -> Double {
        return -number
    }
}
