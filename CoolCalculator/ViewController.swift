import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var resultLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var resultLabelWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!

    var firstNumber: Double = 0
    var performingMathOperation = false
    var operation = 0
    let comma = ","
    let point = "."
    var multiplyButtonWasPressed = false
    var plusButtonWasPressed = false

    func labelIsNotEmpty() -> Bool {
        return label.text != ""
    }

    func clearButtonPressed(_ clearButton: UIButton) -> Bool {
        return clearButton.tag == 11
    }

    func equalButtonPressed(_ sender: UIButton) -> Bool {
        return sender.tag == 16
    }

    func getDoubleValueFromLabel(_ label: UILabel) -> Double {
        let currentLabelText = getCurrentLabelText(label)
            .replacingOccurrences(of: comma, with: point)
        return currentLabelText.isEmpty ? 0 : Double(currentLabelText)!
    }

    func labelTextEndsWithComma() -> Bool {
        return getCurrentLabelText(label).hasSuffix(comma)
    }

    func getCurrentLabelText(_ label: UILabel) -> String {
        return label.text!
    }

    func setLabelText(_ text: String) {
        label.text = text
    }

    func setLabelText(_ text: Double) {
        self.setLabelText(String(text))
    }

    func setLabelText(_ operation: Operations) {
        self.setLabelText(operation.description)
    }

    func clearLabelText() {
        setLabelText("")
    }

    func appendToCurrentLabelText(_ text: String) {
        label.text?.append(text)
    }

    func inverseButtonPressed(_ sender: UIButton) -> Bool {
        return sender.tag == 17
    }

    func percentButtonPressed(_ sender: UIButton) -> Bool {
        return sender.tag == 18
    }

    func divideButtonPressed() -> Bool {
        return operation == 12
    }

    func commaButtonPressed(_ sender: UIButton) -> Bool { return sender.tag == 19 }

    func thereAreCommasOnTheScreen() -> Bool {
        return getCurrentLabelText(label).contains(comma)
    }

    func getTypedNumberValue(_ numberButton: UIButton) -> String {
        return String(numberButton.tag - 1)
    }

    func inverseNumberIfNeeded(_ sender: UIButton) {
        if inverseButtonPressed(sender) {
            firstNumber = Calculator.inverse(firstNumber)
        }
    }

    func outputOperationSign(_ sender: UIButton) {
        switch sender.tag {
        case 12:
            setLabelText(Operations.Divide)
        case 13:
            setLabelText(Operations.Multiply)
            multiplyButtonWasPressed = true
        case 14:
            setLabelText(Operations.Subtract)
        case 15:
            setLabelText(Operations.Plus)
            plusButtonWasPressed = true
        case 17:
            setLabelText(firstNumber)
        default:
            break
        }
        operation = sender.tag
        performingMathOperation = true
    }

    func performMathOperation() {
        let numberOnScreen = getDoubleValueFromLabel(label)
        switch operation {
        case 12:
            setLabelText(Calculator.divide(firstNumber, numberOnScreen))
        case 13:
            setLabelText(Calculator.multiply(firstNumber, numberOnScreen))
        case 14:
            setLabelText(Calculator.subtract(firstNumber, numberOnScreen))
        case 15:
            setLabelText(Calculator.plus(firstNumber, numberOnScreen))
        default:
            break
        }
    }

    @IBAction func numbers(_ sender: UIButton) {
        var typedCharacter = getTypedNumberValue(sender)

        if !performingMathOperation {
            if commaButtonPressed(sender) && labelIsNotEmpty()
                {
                typedCharacter = comma
            }
            appendToCurrentLabelText(typedCharacter)
        }
        else {
            setLabelText(typedCharacter)
            performingMathOperation = false
        }
    }

    @IBAction func buttons(_ sender: UIButton) {
        if clearButtonPressed(sender) {
            clearLabelText()
            firstNumber = 0
        }

        if equalButtonPressed(sender) {
            performMathOperation()
        }

        if labelTextEndsWithComma() {
            return
        }

        let numberOnScreen = getDoubleValueFromLabel(label)

        if percentButtonPressed(sender) && firstNumber != 0 {
            if multiplyButtonWasPressed {
                setLabelText(Calculator.calculatePercent(howMany: numberOnScreen, from: firstNumber))
                multiplyButtonWasPressed = false
            }
            if plusButtonWasPressed {
                setLabelText(Calculator.plusPercent(firstNumber, numberOnScreen))
                plusButtonWasPressed = false
            }
        }

        if labelIsNotEmpty() {
            firstNumber = numberOnScreen
            inverseNumberIfNeeded(sender)
            outputOperationSign(sender)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        label.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

